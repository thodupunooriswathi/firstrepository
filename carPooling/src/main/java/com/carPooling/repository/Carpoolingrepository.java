package com.carPooling.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.carPooling.model.User;

@Repository
public class Carpoolingrepository {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	public static final String CARPOOLING_COLLECTION_NAME="User1";



	public void create(User1 car) {
	  System.out.println("data in rep");
		mongoTemplate.insert(car,CARPOOLING_COLLECTION_NAME);
	}
	public void updatePassword(User1 car) {
		  System.out.println("data in rep");
		  Query query = new Query(Criteria.where("id").is(car.getId()));
		  Update update = new Update();
		  update.set("password", car.getPassword());
		  mongoTemplate.save(car);
	}
	public User1 findByEmail(String email) {
		System.out.println("data in rep");
		Query query = new Query(Criteria.where("email").is(email));
		return mongoTemplate.findOne(query, User1.class,CARPOOLING_COLLECTION_NAME);	
	}
	public List<User1> getAllRiders() {
		Query query = new Query(Criteria.where("userType").is("RIDER"));
		//return mongoTemplate.findAll(query, User.class, CARPOOLING_COLLECTION_NAME);
		return mongoTemplate.find(query, User1.class, CARPOOLING_COLLECTION_NAME);
	}
	public List<User1> getAllProviders() {
		 System.out.println("data in rep");
		Query query = new Query(Criteria.where("userType").is("PROVIDER"));
		return mongoTemplate.find(query, User1.class, CARPOOLING_COLLECTION_NAME);
	}
	public User1 findByid(String id) {
		Query query = new Query(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, User1.class, CARPOOLING_COLLECTION_NAME);
	}
	public void Update(User1 back) {
		mongoTemplate.save(back);
		System.out.println("updated details");
		
		
	}
}
