package com.carPooling.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.carPooling.model.Ridedetails;



@Repository
public class RidedetailsRepositoy {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	public static final String RIDEDETAILS_COLLECTION_NAME="Ridedetails";
	
	public void create(Ridedetails ride){
		System.out.println("data in rep");
		  System.out.println(ride);
		  System.out.println(mongoTemplate);
		  mongoTemplate.insert(ride,RIDEDETAILS_COLLECTION_NAME);
		}

	public Ridedetails findByid(String id) {
		
		System.out.println(id);
		Query query = new Query(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, Ridedetails.class, RIDEDETAILS_COLLECTION_NAME);
	}

	public List<Ridedetails> getAllDetails(String id) {
		System.out.print(id);
		Query query = new Query(Criteria.where("riderid").is(id));
		return mongoTemplate.find(query, Ridedetails.class, RIDEDETAILS_COLLECTION_NAME);
		
	}

	public List<Ridedetails> getproviderid(String id) {
		System.out.print(id);
		Query query = new Query(Criteria.where("providerid").is(id));
		return mongoTemplate.find(query, Ridedetails.class, RIDEDETAILS_COLLECTION_NAME);
		
	}

	public void updateStatus(Ridedetails ride) {
		 System.out.println("data in rep");
		  Update update = new Update();
		  update.set("status", ride.getStatus());
		  mongoTemplate.save(ride);
	}

	public void updateDate(Ridedetails rid) {
		 System.out.println("data in rep");
		  Update update = new Update();
		  update.set("date", rid.getDate());
		  mongoTemplate.save(rid);
		
	}
	
	
	

	public void Update(Ridedetails rideback) {
		mongoTemplate.save(rideback);
		System.out.println("updated time");
		
		
	}

}
