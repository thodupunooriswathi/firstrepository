package com.carPooling.controller;

import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.carPooling.model.RideStatus;
import com.carPooling.model.Ridedetails;
import com.carPooling.model.User1;
import com.carPooling.model.UserStatus;
import com.carPooling.repository.Carpoolingrepository;
import com.carPooling.repository.RidedetailsRepositoy;
import com.carPooling.util.EmailUtil;

@Controller
public class Carpoolingcontroller {
	@Autowired
private Carpoolingrepository carrep;
	@Autowired
	private RidedetailsRepositoy riderep;

	
	@Autowired
	private EmailUtil emailservice;

	Date createTS=new Date();

	@RequestMapping("/Login")
	public String login(Model model  ){
		return "login1";
	}
	
	@RequestMapping("/Register")
	public String Register(Model model){
		model.addAttribute("User1", new User1());
		return "register";
	}
	
	@RequestMapping("/Registration")
	public  String register(@ModelAttribute("User1") User1  car,Model model){
		car.setStatus(UserStatus.ACTIVE);
		car.setCreateTS(createTS);
		car.setUpdateTS(createTS);
		car.setPassword((car.getPassword()));
		Base64.Encoder encode= Base64.getEncoder();
		    car.setPassword(encode.encodeToString(car.getPassword().getBytes()));
		
		carrep.create(car);
		return "Home";
	}
	
	@RequestMapping("/logincheck")
	public String check(String email,String password,HttpSession session)throws Exception
	{
		
		System.out.println(email);	
		User1 usr =carrep.findByEmail(email);
		
		Base64.Encoder encode= Base64.getEncoder();
		
		if(usr!=null){
			if(usr.getPassword().equals(encode.encodeToString(password.getBytes())))
			{
				if(usr.getUserType().toString().equals("RIDER"))
				{
					session.setAttribute("usrid",usr.getId());
					
				return "Takeride";
				}
			
			else
			{
				session.setAttribute("usrid", usr.getId());
				return "Acceptride";
				
			}
			}
			else{
				throw new Exception("Invalid Login"); 
				}

		}
				else{
					throw new Exception("No User");
				
				}
		}
		
		
	@RequestMapping("/RiderList")
	public String getAllRiders(Model model, String id){
	return "home";
	}
	
	@RequestMapping("/RideList")
	public String RequestRide(@ModelAttribute("User1") User1 user,Model model,HttpSession session){
		  String id=(String) session.getAttribute("usrid");
		 System.out.println(id);
    System.out.println("data in cnt");
    List<Ridedetails> ride= riderep.getproviderid(id);
	model.addAttribute("ride",ride);
	System.out.println(ride);
	return "ViewRider";
	}
	
	@RequestMapping("/ProviderList")
	public String getAllProviders(Model model){
    System.out.println("data in cnt");
	List<User1> providerList= carrep.getAllProviders();
	model.addAttribute("providerList",providerList);
	System.out.println(providerList);
	return "ViewProvide";
	}
	
	@RequestMapping("/Takearide")
	public String Takearide(Model model  ){
		return "Takeride";
	}	
	
	@RequestMapping("/Acceptride")
	public String Acceptride(Model model  ){
		return "Acceptride";
	}	
	
	@RequestMapping("/Ridereq")
	public String Ridereq(Model model,String providerid, HttpSession session ){
		model.addAttribute("providerid",providerid);
		session.setAttribute("providerid", providerid);
		System.out.println(providerid);
		return "Riderequest";
	}
	
	@RequestMapping("/Riderequests")
	public String Riderequests( @ModelAttribute("Ridedetails") Ridedetails ride,String providerid,Model model,HttpSession session  ){
		System.out.println(providerid);
		User1 user =carrep.findByid(session.getAttribute("providerid").toString());
		ride.setProviderid(user.getId());
		ride.setProvidername(user.getLastname());
		ride.setProvidermobile(user.getMobileno());
		ride.setCost(user.getPrice());
		String userId =(String) session.getAttribute("usrid");
		User1 usr =carrep.findByid(userId);
		System.out.println(usr);
		//ride.setId(usr.getId());
		ride.setRiderid(usr.getId());
		ride.setRidername(usr.getLastname());
		ride.setRidermobile(usr.getMobileno());
		ride.setStatus(RideStatus.PENDING);
		ride.setCreateTS(createTS);
		ride.setUpdateTS(createTS);
		System.out.println("data in con");
		riderep.create(ride);
		return "Takeride";
	}
	
	@RequestMapping("/Ridedetails")
	public String getAllDetails(Model model,HttpSession session){
		 String id=(String) session.getAttribute("usrid");
		System.out.println(id);
    System.out.println("data in cnt");
	List<Ridedetails> ride= riderep.getAllDetails(id);
	model.addAttribute("ride",ride);
	System.out.println(ride);
	return "Ridedetails";
	}
		
@RequestMapping("/forgotpassword")
public String forgot(Model model){
	return "forgotpassword";
}
@RequestMapping("/forgetpassword")

	public String forgetpassword(String email){
		System.out.println(email);
		String verificationToken = UUID.randomUUID().toString();
	User1 usr  =carrep.findByEmail(email);
		usr.setVerificationtoken(verificationToken);
		carrep.Update(usr);
		try {
			emailservice.sendEmailForPasswordReset(usr.getLastname(), usr.getEmail(), verificationToken, usr.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		  return "home";
}
       
	



@RequestMapping("/resetpassword")
public String reset( String uid,String token,Model model){
	model.addAttribute("uid",uid);
		model.addAttribute("token",token);
		return "resetpassword";
	
}

@RequestMapping("/reset")
public String resetPassword(String uid, String token, String password, Model model){
	User1 usr  =carrep.findByid(uid);
   Base64.Encoder encode= Base64.getEncoder();
		if( usr!=null && (usr.getVerificationtoken().equals(token)||!token.equals("")))
		{
			usr.setPassword(encode.encodeToString(password.getBytes()));
			usr.setVerificationtoken(" ");
			usr.setUpdateTS(createTS);
			carrep.Update(usr);
			
		System.out.println("Password set...");
		}
		else{
		System.out.println("password not reset");
		}
		return "login1";
		}




@RequestMapping("/change")
public String changpwd(Model model){
	return "changepassword1";
}

@RequestMapping("/changepassword")
public String change(String  oldpassword ,String newpassword,HttpSession session)throws Exception{
System.out.println(oldpassword);
	String userId =(String) session.getAttribute("usrid");
	User1 user  =carrep.findByid(userId);
	if(user!=null)
	{
		if(oldpassword.equals(user.getPassword()))
		{
			user.setPassword(newpassword);
			System.out.println(newpassword);
			user.setUpdateTS(createTS);
			carrep.updatePassword(user);
			return "login1";
		}
		else{
			throw new Exception("password mismatches"); 
			}
	}
	else{
		throw new Exception("No user"); 
		}
}
@RequestMapping("/changepwd")
public String changpwd1(Model model){
	return "changepasswordprovider";
}



@RequestMapping("/AcceptRequest")
public String AcceptRequest(Model model,String id){
	System.out.println(id);
	Ridedetails rid=riderep.findByid(id);
	System.out.println(rid);
	rid.setStatus(RideStatus.ACCEPT);
	rid.setUpdateTS(createTS);
	riderep.updateStatus(rid);
	return "Acceptride";
}

@RequestMapping("/DeclineRequest")
public String DeclineRequest(Model model,String id){
	System.out.println(id);
	Ridedetails rid=riderep.findByid(id);
	System.out.println(rid);
	rid.setStatus(RideStatus.DECLINE);
	rid.setUpdateTS(createTS);
	riderep.updateStatus(rid);
	return "Acceptride";	
}

@RequestMapping("/logout")
public String logout(Model model){
	return "home";
}

@RequestMapping("/edittime")
public String edittime(Model model ,String id){
	System.out.print(id);
	Ridedetails rideData=riderep.findByid(id);
	model.addAttribute("rideData",rideData);
	return "Updatetime";
}	
@RequestMapping("/updatetime")
public String updatetime(@ModelAttribute("Ridedetails") Ridedetails ride,Model model ){
Ridedetails rideback=riderep.findByid(ride.getId());
System.out.println(rideback);
if(rideback!=null)	
{
rideback.setDate(ride.getDate());
System.out.print(rideback);
ride.setUpdateTS(createTS);
riderep.Update(rideback);	
}
Ridedetails rideData=riderep.findByid(ride.getId());
model.addAttribute("ride",rideData);
return "ViewRider";
}



@RequestMapping("/edituserdetails")
public String editdetails(Model model,HttpSession session){
	String id =(String) session.getAttribute("usrid");
	System.out.println(id);
	User1 user=carrep.findByid(id);
	model.addAttribute("User", new User1());
	model.addAttribute("user",user);
	return "Updatedetails";
}

@RequestMapping("/updatedetails")
public String updatedetails(@ModelAttribute("User") User1 user,Model model ){
User1 back=carrep.findByid(user.getId());
System.out.println(user);
if(back!=null)	
{
	back.setFirstname(user.getFirstname());
	back.setLastname(user.getLastname());
	back.setEmail(user.getEmail());
	back.setMobileno(user.getMobileno());
	back.setAadharno(user.getAadharno());
	back.setAge(user.getAge());
	back.setUserType(user.getUserType());
	back.setFrom(user.getFrom());
	back.setTo(user.getTo());
	back.setDate(user.getDate());
	back.setVehicleno(user.getVehicleno());
	back.setDrivinglicense(user.getDrivinglicense());
	back.setDistance(user.getDistance());
	back.setPrice(user.getPrice());
	back.setAddress(user.getAddress());
	user.setUpdateTS(createTS);
System.out.print(back);
carrep.Update(back);	
}
return "Takeride";
}
@RequestMapping("/editproviderdetails")
public String editprovdetails(Model model,HttpSession session){
	String id =(String) session.getAttribute("usrid");
	System.out.println(id);
	User1 user=carrep.findByid(id);
	model.addAttribute("User", new User1());
	model.addAttribute("user",user);
	return "updateproviderdetails";
}
@RequestMapping("/updateprovdetails")
public String updateprovdetails(@ModelAttribute("User") User1 user,Model model ){
User1 back=carrep.findByid(user.getId());
System.out.println(user);
if(back!=null)	
{
	back.setFirstname(user.getFirstname());
	back.setLastname(user.getLastname());
	back.setEmail(user.getEmail());
	back.setMobileno(user.getMobileno());
	back.setAadharno(user.getAadharno());
	back.setAge(user.getAge());
	back.setUserType(user.getUserType());
	back.setFrom(user.getFrom());
	back.setTo(user.getTo());
	back.setDate(user.getDate());
	back.setVehicleno(user.getVehicleno());
	back.setDrivinglicense(user.getDrivinglicense());
	back.setDistance(user.getDistance());
	back.setPrice(user.getPrice());
	back.setAddress(user.getAddress());
	user.setUpdateTS(createTS);
System.out.print(back);
carrep.Update(back);	
}
return "Acceptride";
}

}


		
	
	


