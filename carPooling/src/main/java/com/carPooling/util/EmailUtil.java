package com.carPooling.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.carPooling.model.User1;




@Service
public class EmailUtil {
	
	@Value("${passwordreseturl}")
	private String passwordreseturl;
	
Session session;
	
@Autowired
public EmailUtil(@Value ("${smtp.smtpUserName}") final String smtpUserName,			
			     @Value ("${smtp.smtpPassword}") final String smtpPassword,
			     @Value ("${smtp.smtpHost}") String smtpHost,
			     @Value ("${smtp.smtpPort}") String smtpPort) {
	Properties props = new Properties();						
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.starttls.enable", "true");
	props.put("mail.smtp.host", smtpHost);
	props.put("mail.smtp.port", smtpPort);
	props.put("mail.transport.protocol","smtp" );
	props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
	props.put("mail.smtp.socketFactory.fallback","false");			
	session = Session.getInstance(props, new javax.mail.Authenticator() {
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(smtpUserName, smtpPassword);	
		}
	});
}
	
	
	
	public void sendEmail(User1 user) throws Exception {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("telekha.tracker@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(user.getEmail()));
			message.setSubject("successfully Registion");
			StringBuffer messageBody = new StringBuffer();
			messageBody.append("Hi ");
			messageBody.append(user.getLastname());
			messageBody.append("<br />");
			messageBody.append("Your successfully register.");
			messageBody.append("<br />");
			messageBody.append("Regards,");	
			messageBody.append("<br />");
			messageBody.append("Telekha Team");	
			message.setContent(messageBody.toString(), "text/html");			
			Transport.send(message);	
		}
		catch (MessagingException e) {			
			throw new Exception("Error while sending mail");
		}
	}
	
	public void sendEmailForPasswordReset(String name, String email, String verificationToken, String id
			) throws Exception {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("telekha.tracker@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(email));
			message.setSubject("Forgotpassword");
			String url = passwordreseturl.replace("{uid}", id).replace("{token}", verificationToken);
			StringBuffer messageBody = new StringBuffer();
			messageBody.append("Hi ");
			messageBody.append(name);
			messageBody.append(",<br /><br />");
			messageBody.append("We have received a password reset request from you. Please <a href=\"");
			messageBody.append(url);
			messageBody.append("\" target=\"_blank\"> click </a> here to reset the password. <br />");
			messageBody.append("If the link is not accessible, please copy paste the url in browser ");
			messageBody.append(url);
			messageBody.append("<br />");
			messageBody.append("If you have not requested for password reset, please forward this mail to support@cpm.com");
			messageBody.append("<br /><br />");
			messageBody.append("Regards,<br />Telekha Team");
			message.setContent(messageBody.toString(), "text/html");			
			Transport.send(message);
			
			
		} catch (Exception e) {
			throw new Exception("Error while sending mail");
		}

}
}
