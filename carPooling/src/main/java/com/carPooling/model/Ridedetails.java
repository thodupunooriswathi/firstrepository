package com.carPooling.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Ridedetails")
public class Ridedetails {

@Id
private String id;
private String riderid;
private String ridername;
private String ridermobile;
private String providerid;
private String providername;
private String providermobile;
private String cost;
private String from;
private String to;
private String date;
private RideStatus status;
private Date createTS;
private Date updateTS;

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public String getRiderid() {
	return riderid;
}

public void setRiderid(String riderid) {
	this.riderid = riderid;
}

public String getRidername() {
	return ridername;
}

public void setRidername(String ridername) {
	this.ridername = ridername;
}

public String getRidermobile() {
	return ridermobile;
}

public void setRidermobile(String ridermobile) {
	this.ridermobile = ridermobile;
}

public String getProviderid() {
	return providerid;
}

public void setProviderid(String providerid) {
	this.providerid = providerid;
}

public String getProvidername() {
	return providername;
}

public void setProvidername(String providername) {
	this.providername = providername;
}

public String getProvidermobile() {
	return providermobile;
}

public void setProvidermobile(String providermobile) {
	this.providermobile = providermobile;
}

public String getCost() {
	return cost;
}

public void setCost(String cost) {
	this.cost = cost;
}

public String getFrom() {
	return from;
}

public void setFrom(String from) {
	this.from = from;
}

public String getTo() {
	return to;
}

public void setTo(String to) {
	this.to = to;
}

public String getDate() {
	return date;
}

public void setDate(String date) {
	this.date = date;
}

public RideStatus getStatus() {
	return status;
}

public void setStatus(RideStatus status) {
	this.status = status;
}

public Date getCreateTS() {
	return createTS;
}

public void setCreateTS(Date createTS) {
	this.createTS = createTS;
}

public Date getUpdateTS() {
	return updateTS;
}

public void setUpdateTS(Date updateTS) {
	this.updateTS = updateTS;
}


}
