package com.carPooling.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="User1")
public class User1 {
	
@Id
private String id;
private	String firstname;
private String lastname;
private	String email;
private String password;
private	String mobileno;
private	String aadharno;
private	String age;
private Usertype userType;
private	String from;
private	String to;
private String date;
private String vehicleno;
private Drivinglicense drivinglicense;
private String distance;
private String price;
private	String address;
private String gender;
private	UserStatus status;
private String verificationtoken;
private Date createTS;
private Date updateTS;


public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getMobileno() {
	return mobileno;
}
public void setMobileno(String mobileno) {
	this.mobileno = mobileno;
}
public String getAadharno() {
	return aadharno;
}
public void setAadharno(String aadharno) {
	this.aadharno = aadharno;
}
public String getAge() {
	return age;
}
public void setAge(String age) {
	this.age = age;
}
public Usertype getUserType() {
	return userType;
}
public void setUserType(Usertype userType) {
	this.userType = userType;
}
public String getFrom() {
	return from;
}
public void setFrom(String from) {
	this.from = from;
}
public String getTo() {
	return to;
}
public void setTo(String to) {
	this.to = to;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}

public String getVehicleno() {
	return vehicleno;
}
public void setVehicleno(String vehicleno) {
	this.vehicleno = vehicleno;
}


public Drivinglicense getDrivinglicense() {
	return drivinglicense;
}
public void setDrivinglicense(Drivinglicense drivinglicense) {
	this.drivinglicense = drivinglicense;
}
public String getDistance() {
	return distance;
}
public void setDistance(String distance) {
	this.distance = distance;
}
public String getPrice() {
	return price;
}
public void setPrice(String price) {
	this.price = price;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public UserStatus getStatus() {
	return status;
}
public void setStatus(UserStatus status) {
	this.status = status;
}
public Date getCreateTS() {
	return createTS;
}
public void setCreateTS(Date createTS) {
	this.createTS = createTS;
}
public Date getUpdateTS() {
	return updateTS;
}
public void setUpdateTS(Date updateTS) {
	this.updateTS = updateTS;
}
public String getVerificationtoken() {
	return verificationtoken;
}
public void setVerificationtoken(String verificationtoken) {
	this.verificationtoken = verificationtoken;
}




}
